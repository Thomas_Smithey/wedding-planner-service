/**
 * This is the same error that Adam designed in the LibraryAPI project. 
 * It works well with this project since there are many times that an id value 
 * is passed into the database via a query. If the id value passed in does not 
 * exist in the tables of the database then an error needs to be thrown and a 
 * message displayed to the user.
 */

 export class MissingResourceError
 {
     message:string;
     description:string = "This error means a resource could not be found";
 
     constructor(message:string)
     {
         this.message = message;
     }
 }