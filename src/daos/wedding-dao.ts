import { Wedding } from "../entities";

/**
 * This is the Wedding Data Access Object. Here, the interface is
 * declared with all of the necessary functions to perform CRUD 
 * operations on the wedding table. These functions will 
 * return promises that contain wedding objects.
 */

export interface WeddingDAO
{
    createWedding(wedding:Wedding):Promise<Wedding>; // CREATE
    getAllWeddings():Promise<Wedding[]>; // READ
    getWeddingById(id:number):Promise<Wedding>; // READ
    updateWedding(wedding:Wedding):Promise<Wedding>; // UPDATE
    deleteWeddingById(id:number):Promise<boolean>; // DELETE
}