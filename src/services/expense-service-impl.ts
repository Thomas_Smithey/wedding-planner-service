import { ExpenseDAO } from "../daos/expense-dao";
import { ExpenseDaoPostgres } from "../daos/expense-dao-postgres-impl";
import { Expense } from "../entities";
import ExpenseService from "./expense-service";

/**
 * This is the implementation of ExpenseService. This is
 * the service layer where we could add in business logic.
 */

export class ExpenseServiceImpl implements ExpenseService
{
    expenseDao:ExpenseDAO = new ExpenseDaoPostgres();

    registerExpense(expense:Expense):Promise<Expense> // CREATE, POST
    {
        return this.expenseDao.createExpense(expense);
    }

    retrieveAllExpenses():Promise<Expense[]> // READ, GET
    {
        return this.expenseDao.getAllExpenses();
    }

    retrieveExpenseById(id:number):Promise<Expense> // READ, GET
    {
        return this.expenseDao.getExpenseById(id);
    }

    modifyExpense(expense:Expense):Promise<Expense> // UPDATE, PUT, PATCH
    {
        return this.expenseDao.updateExpense(expense);
    }

    removeExpenseById(id:number):Promise<boolean> // DELETE
    {
        return this.expenseDao.deleteExpenseById(id);
    }
}