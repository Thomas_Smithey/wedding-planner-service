import { Expense } from "../entities";

/**
 * This is the service layer. An interface is made for
 * expense services. This is where the services functions
 * are declared. Note that these functions have similar
 * naming conventions to those of the ExpenseDAO. This
 * is because we will be accessing the DAO through
 * service layer functions.
 */

export default interface ExpenseService
{
    registerExpense(expense:Expense):Promise<Expense>;
    retrieveAllExpenses():Promise<Expense[]>;
    retrieveExpenseById(id:number):Promise<Expense>;
    modifyExpense(expense:Expense):Promise<Expense>;
    removeExpenseById(id:number):Promise<boolean>;
}