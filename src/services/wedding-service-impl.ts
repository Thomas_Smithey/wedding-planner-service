import { WeddingDAO } from "../daos/wedding-dao";
import { WeddingDaoPostgres } from "../daos/wedding-dao-postgres-impl";
import { Wedding } from "../entities";
import WeddingService from "./wedding-service";

/**
 * This is the implementation of WeddingService. This is
 * the service layer where we could add in business logic.
 */

export class WeddingServiceImpl implements WeddingService
{
    weddingDao:WeddingDAO = new WeddingDaoPostgres();

    registerWedding(wedding:Wedding):Promise<Wedding> // CREATE, POST
    {
        return this.weddingDao.createWedding(wedding);
    }

    retrieveAllWeddings():Promise<Wedding[]> // READ, GET
    {
        return this.weddingDao.getAllWeddings();
    }

    retrieveWeddingById(id:number):Promise<Wedding> // READ, GET
    {
        return this.weddingDao.getWeddingById(id);
    }

    modifyWedding(client:Wedding):Promise<Wedding> // UPDATE, PUT, PATCH
    {
        return this.weddingDao.updateWedding(client);
    }

    removeWeddingById(id:number):Promise<boolean> // DELETE
    {
        return this.weddingDao.deleteWeddingById(id);
    }
}