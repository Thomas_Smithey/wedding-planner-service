import { Wedding } from "../entities";

/**
 * This is the service layer. An interface is made for
 * wedding services. This is where the services functions
 * are declared. Note that these functions have similar
 * naming conventions to those of the WeddingDAO. This
 * is because we will be accessing the DAO through
 * service layer functions.
 */

export default interface WeddingService
{
    registerWedding(wedding:Wedding):Promise<Wedding>;
    retrieveAllWeddings():Promise<Wedding[]>;
    retrieveWeddingById(id:number):Promise<Wedding>;
    modifyWedding(wedding:Wedding):Promise<Wedding>;
    removeWeddingById(id:number):Promise<boolean>;
}