export class Wedding
{
    constructor
    (
        public id:number,
        public w_date:string,
        public w_location:string,
        public w_name:string,
        public budget:number
    ){};
}

export class Expense
{
    constructor
    (
        public id:number,
        public w_id:number,
        public reason:string,
        public amount:number,
        public photo:string
    ){};
}