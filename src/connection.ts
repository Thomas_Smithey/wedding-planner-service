import { Client } from "pg";

/**
 * Import the Client module from the node-postgres
 * collection. This is used to connect to the database.
 * Here, the variable conn becomes an instance of
 * Client. This variable may be passed to any file.
 */

export const conn = new Client
({
    user:"postgres",
    password:process.env.DBPASSWORD,
    database:"weddingdb",
    port:5432,
    host:"34.132.148.74"
});

conn.connect();