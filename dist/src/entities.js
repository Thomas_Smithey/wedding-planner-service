"use strict";
exports.__esModule = true;
exports.Expense = exports.Wedding = void 0;
var Wedding = /** @class */ (function () {
    function Wedding(id, w_date, w_location, w_name, budget) {
        this.id = id;
        this.w_date = w_date;
        this.w_location = w_location;
        this.w_name = w_name;
        this.budget = budget;
    }
    ;
    return Wedding;
}());
exports.Wedding = Wedding;
var Expense = /** @class */ (function () {
    function Expense(id, w_id, reason, amount, photo) {
        this.id = id;
        this.w_id = w_id;
        this.reason = reason;
        this.amount = amount;
        this.photo = photo;
    }
    ;
    return Expense;
}());
exports.Expense = Expense;
