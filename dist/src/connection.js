"use strict";
exports.__esModule = true;
exports.conn = void 0;
var pg_1 = require("pg");
/**
 * Import the Client module from the node-postgres
 * collection. This is used to connect to the database.
 * Here, the variable conn becomes an instance of
 * Client. This variable may be passed to any file.
 */
exports.conn = new pg_1.Client({
    user: "postgres",
    password: process.env.DBPASSWORD,
    database: "weddingdb",
    port: 5432,
    host: "34.132.148.74"
});
exports.conn.connect();
