"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var express_1 = __importDefault(require("express"));
var wedding_service_impl_1 = require("./services/wedding-service-impl");
var expense_service_impl_1 = require("./services/expense-service-impl");
var errors_1 = require("./errors");
var cors_1 = __importDefault(require("cors"));
var app = (0, express_1["default"])();
app.use(express_1["default"].json());
app.use((0, cors_1["default"])());
var weddingService = new wedding_service_impl_1.WeddingServiceImpl();
var expenseService = new expense_service_impl_1.ExpenseServiceImpl();
/**
GET /weddings
GET /weddings/:id
GET /weddings/:id/expenses
POST /weddings
DELETE /weddings/:id
PUT /weddings
 */
app.get("/weddings", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var weddings;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, weddingService.retrieveAllWeddings()];
            case 1:
                weddings = _a.sent();
                res.status(200);
                res.send(weddings);
                return [2 /*return*/];
        }
    });
}); });
app.get("/weddings/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var wedding, error_1;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, weddingService.retrieveWeddingById(Number(req.params.id))];
            case 1:
                wedding = _a.sent();
                res.status(200);
                res.send(wedding);
                return [2 /*return*/];
            case 2:
                error_1 = _a.sent();
                if (error_1 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_1);
                    return [2 /*return*/];
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.get("/weddings/:id/expenses", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var wedding, error_2, allExpenses, expenses, _i, allExpenses_1, expense;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, weddingService.retrieveWeddingById(Number(req.params.id))];
            case 1:
                wedding = _a.sent();
                return [3 /*break*/, 3];
            case 2:
                error_2 = _a.sent();
                if (error_2 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_2);
                    return [2 /*return*/];
                }
                return [3 /*break*/, 3];
            case 3: return [4 /*yield*/, expenseService.retrieveAllExpenses()];
            case 4:
                allExpenses = _a.sent();
                expenses = [];
                for (_i = 0, allExpenses_1 = allExpenses; _i < allExpenses_1.length; _i++) {
                    expense = allExpenses_1[_i];
                    if (expense.w_id === Number(req.params.id)) {
                        expenses.push(expense);
                    }
                }
                res.status(200);
                res.send(expenses);
                return [2 /*return*/];
        }
    });
}); });
app.post("/weddings", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var wedding;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, weddingService.registerWedding(req.body)];
            case 1:
                wedding = _a.sent();
                res.status(201);
                res.send(wedding);
                return [2 /*return*/];
        }
    });
}); });
app["delete"]("/weddings/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var wedding, error_3;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, weddingService.removeWeddingById(Number(req.params.id))];
            case 1:
                wedding = _a.sent();
                res.status(205);
                res.send(wedding);
                return [2 /*return*/];
            case 2:
                error_3 = _a.sent();
                if (error_3 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_3);
                    return [2 /*return*/];
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.put("/weddings", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var wedding, error_4;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, weddingService.modifyWedding(req.body)];
            case 1:
                wedding = _a.sent();
                res.status(200);
                res.send(wedding);
                return [2 /*return*/];
            case 2:
                error_4 = _a.sent();
                if (error_4 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_4);
                    return [2 /*return*/];
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
/**
GET /expenses
GET /expenses/:id
POST /expenses
PUT /expenses/:id
DELETE /expenses/:id
 */
app.get("/expenses", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var expenses;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, expenseService.retrieveAllExpenses()];
            case 1:
                expenses = _a.sent();
                res.status(200);
                res.send(expenses);
                return [2 /*return*/];
        }
    });
}); });
app.get("/expenses/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var expense, error_5;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, expenseService.retrieveExpenseById(Number(req.params.id))];
            case 1:
                expense = _a.sent();
                res.status(200);
                res.send(expense);
                return [2 /*return*/];
            case 2:
                error_5 = _a.sent();
                if (error_5 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_5);
                    return [2 /*return*/];
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
app.post("/expenses", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var expense;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, expenseService.registerExpense(req.body)];
            case 1:
                expense = _a.sent();
                res.status(201);
                res.send(expense);
                return [2 /*return*/];
        }
    });
}); });
app.put("/expenses/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var testExpense, expense, error_6;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 3, , 4]);
                return [4 /*yield*/, expenseService.retrieveExpenseById(Number(req.params.id))];
            case 1:
                testExpense = _a.sent();
                if (Number(req.params.id) !== req.body.id) {
                    res.status(403);
                    res.send({ "message": "Error: The id in the url (" + Number(req.params.id) + ") does not match the id in the body (" + req.body.id + ")." });
                    return [2 /*return*/];
                }
                if (testExpense.w_id !== req.body["w_id"]) {
                    res.status(403);
                    res.send("Error: expense with id " + testExpense.id + " belongs to the wedding with id " + testExpense.w_id + ". Cannot modify w_id.");
                    return [2 /*return*/];
                }
                return [4 /*yield*/, expenseService.modifyExpense(req.body)];
            case 2:
                expense = _a.sent();
                res.status(200);
                res.send(expense);
                return [2 /*return*/];
            case 3:
                error_6 = _a.sent();
                if (error_6 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_6);
                    return [2 /*return*/];
                }
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
app["delete"]("/expenses/:id", function (req, res) { return __awaiter(void 0, void 0, void 0, function () {
    var expense, error_7;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                _a.trys.push([0, 2, , 3]);
                return [4 /*yield*/, expenseService.removeExpenseById(Number(req.params.id))];
            case 1:
                expense = _a.sent();
                res.status(205);
                res.send(expense);
                return [2 /*return*/];
            case 2:
                error_7 = _a.sent();
                if (error_7 instanceof errors_1.MissingResourceError) {
                    res.status(404);
                    res.send(error_7);
                    return [2 /*return*/];
                }
                return [3 /*break*/, 3];
            case 3: return [2 /*return*/];
        }
    });
}); });
// listen for this application on port 3000.
app.listen(3001, function () { console.log("Application started"); });
