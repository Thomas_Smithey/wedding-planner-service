"use strict";
exports.__esModule = true;
exports.ExpenseServiceImpl = void 0;
var expense_dao_postgres_impl_1 = require("../daos/expense-dao-postgres-impl");
/**
 * This is the implementation of ExpenseService. This is
 * the service layer where we could add in business logic.
 */
var ExpenseServiceImpl = /** @class */ (function () {
    function ExpenseServiceImpl() {
        this.expenseDao = new expense_dao_postgres_impl_1.ExpenseDaoPostgres();
    }
    ExpenseServiceImpl.prototype.registerExpense = function (expense) {
        return this.expenseDao.createExpense(expense);
    };
    ExpenseServiceImpl.prototype.retrieveAllExpenses = function () {
        return this.expenseDao.getAllExpenses();
    };
    ExpenseServiceImpl.prototype.retrieveExpenseById = function (id) {
        return this.expenseDao.getExpenseById(id);
    };
    ExpenseServiceImpl.prototype.modifyExpense = function (expense) {
        return this.expenseDao.updateExpense(expense);
    };
    ExpenseServiceImpl.prototype.removeExpenseById = function (id) {
        return this.expenseDao.deleteExpenseById(id);
    };
    return ExpenseServiceImpl;
}());
exports.ExpenseServiceImpl = ExpenseServiceImpl;
