"use strict";
exports.__esModule = true;
exports.WeddingServiceImpl = void 0;
var wedding_dao_postgres_impl_1 = require("../daos/wedding-dao-postgres-impl");
/**
 * This is the implementation of WeddingService. This is
 * the service layer where we could add in business logic.
 */
var WeddingServiceImpl = /** @class */ (function () {
    function WeddingServiceImpl() {
        this.weddingDao = new wedding_dao_postgres_impl_1.WeddingDaoPostgres();
    }
    WeddingServiceImpl.prototype.registerWedding = function (wedding) {
        return this.weddingDao.createWedding(wedding);
    };
    WeddingServiceImpl.prototype.retrieveAllWeddings = function () {
        return this.weddingDao.getAllWeddings();
    };
    WeddingServiceImpl.prototype.retrieveWeddingById = function (id) {
        return this.weddingDao.getWeddingById(id);
    };
    WeddingServiceImpl.prototype.modifyWedding = function (client) {
        return this.weddingDao.updateWedding(client);
    };
    WeddingServiceImpl.prototype.removeWeddingById = function (id) {
        return this.weddingDao.deleteWeddingById(id);
    };
    return WeddingServiceImpl;
}());
exports.WeddingServiceImpl = WeddingServiceImpl;
