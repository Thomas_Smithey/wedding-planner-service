"use strict";
/**
 * This is the same error that Adam designed in the LibraryAPI project.
 * It works well with this project since there are many times that an id value
 * is passed into the database via a query. If the id value passed in does not
 * exist in the tables of the database then an error needs to be thrown and a
 * message displayed to the user.
 */
exports.__esModule = true;
exports.MissingResourceError = void 0;
var MissingResourceError = /** @class */ (function () {
    function MissingResourceError(message) {
        this.description = "This error means a resource could not be found";
        this.message = message;
    }
    return MissingResourceError;
}());
exports.MissingResourceError = MissingResourceError;
