"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var connection_1 = require("../src/connection");
var wedding_dao_postgres_impl_1 = require("../src/daos/wedding-dao-postgres-impl");
var entities_1 = require("../src/entities");
/**
 * This is a series of jest tests that ensure CRUD
 * operations can be performed on the tables in the
 * database and that the WeddingDAO functions are
 * working.
 *
 * Both tables wedding and expense must exist before
 * running these tests for a successful outcome.
 *
 * If any records were deleted before running
 * these tests then it is best to drop and
 * recreate both tables to ensure there are
 * no orphan records.
 */
var weddingDAO = new wedding_dao_postgres_impl_1.WeddingDaoPostgres();
// Tests the createWedding function in the WeddingDAO.
test("Create a wedding", function () { return __awaiter(void 0, void 0, void 0, function () {
    var testWedding, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                testWedding = new entities_1.Wedding(1, "2021-10-15", "11111 W 90th St.", "Thomas's wedding", 30000);
                return [4 /*yield*/, weddingDAO.createWedding(testWedding)];
            case 1:
                result = _a.sent();
                expect(result.id).not.toBe(0);
                return [2 /*return*/];
        }
    });
}); });
// Tests the getAllWeddings function in the WeddingDAO.
test("Get all weddings", function () { return __awaiter(void 0, void 0, void 0, function () {
    var w1, w2, w3, weddings;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                w1 = new entities_1.Wedding(1, "2021-10-16", "11111 W 90th St.", "Amy's wedding", 30000);
                w2 = new entities_1.Wedding(1, "2021-10-17", "11111 W 90th St.", "Bob's wedding", 30000);
                w3 = new entities_1.Wedding(1, "2021-10-18", "11111 W 90th St.", "Stacy's wedding", 30000);
                return [4 /*yield*/, weddingDAO.createWedding(w1)];
            case 1:
                _a.sent();
                return [4 /*yield*/, weddingDAO.createWedding(w2)];
            case 2:
                _a.sent();
                return [4 /*yield*/, weddingDAO.createWedding(w3)];
            case 3:
                _a.sent();
                return [4 /*yield*/, weddingDAO.getAllWeddings()];
            case 4:
                weddings = _a.sent();
                expect(weddings.length).toBeGreaterThanOrEqual(3);
                return [2 /*return*/];
        }
    });
}); });
// Tests the getWeddingById function in the WeddingDAO.
test("Get wedding by id", function () { return __awaiter(void 0, void 0, void 0, function () {
    var wedding, retrievedWedding;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                wedding = new entities_1.Wedding(1, "2021-10-15", "11111 W 90th St.", "Thomas's wedding", 30000);
                return [4 /*yield*/, weddingDAO.createWedding(wedding)];
            case 1:
                wedding = _a.sent();
                return [4 /*yield*/, weddingDAO.getWeddingById(wedding.id)];
            case 2:
                retrievedWedding = _a.sent();
                expect(retrievedWedding.id).toBe(wedding.id);
                return [2 /*return*/];
        }
    });
}); });
// Tests the updateWedding function in the WeddingDAO.
test("Update wedding", function () { return __awaiter(void 0, void 0, void 0, function () {
    var wedding;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                wedding = new entities_1.Wedding(1, "2021-10-15", "11111 W 90th St.", "Tom's wedding", 30000);
                return [4 /*yield*/, weddingDAO.createWedding(wedding)];
            case 1:
                wedding = _a.sent();
                wedding.budget = 40000;
                return [4 /*yield*/, weddingDAO.updateWedding(wedding)];
            case 2:
                wedding = _a.sent();
                expect(wedding.budget).toBe(40000);
                return [2 /*return*/];
        }
    });
}); });
// Tests the deleteWeddingById function in the WeddingDAO.
test("Delete wedding by id", function () { return __awaiter(void 0, void 0, void 0, function () {
    var wedding, result;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                wedding = new entities_1.Wedding(1, "2021-10-15", "11111 W 90th St.", "Tomas's wedding", 30000);
                return [4 /*yield*/, weddingDAO.createWedding(wedding)];
            case 1:
                wedding = _a.sent();
                return [4 /*yield*/, weddingDAO.deleteWeddingById(wedding.id)];
            case 2:
                result = _a.sent();
                expect(result).toBeTruthy();
                return [2 /*return*/];
        }
    });
}); });
// Closes the connection after the tests finish.
afterAll(function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        connection_1.conn.end();
        return [2 /*return*/];
    });
}); });
