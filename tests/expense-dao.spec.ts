import { conn } from "../src/connection";
import { ExpenseDAO } from "../src/daos/expense-dao";
import { ExpenseDaoPostgres } from "../src/daos/expense-dao-postgres-impl";
import { Expense } from "../src/entities";

/**
 * This is a series of jest tests that ensure CRUD
 * operations can be performed on the tables in the
 * database and that the ExpenseDAO functions are
 * working.
 * 
 * Both tables wedding and expense must exist before
 * running these tests for a successful outcome.
 * 
 * If any records were deleted before running
 * these tests then it is best to drop and 
 * recreate both tables to ensure there are 
 * no orphan records.
 */

const expenseDao:ExpenseDAO = new ExpenseDaoPostgres();

// Tests the createExpense function in the ExpenseDAO.
test("Create an expense", async () =>
{
    const testExpense = new Expense(1, 1, "for more chairs", 200, "");
    const result = await expenseDao.createExpense(testExpense);
    expect(result.id).not.toBe(0);
});

// Tests the getAllExpenses function in the ExpenseDAO.
test("Get all expenses", async () =>
{
    let e1:Expense = new Expense(1, 1, "for more food", 500, "");
    let e2:Expense = new Expense(1, 2, "for more drinks", 300, "");
    let e3:Expense = new Expense(1, 3, "for even more chairs", 200, "");

    await expenseDao.createExpense(e1);
    await expenseDao.createExpense(e2);
    await expenseDao.createExpense(e3);

    const expenses:Expense[] = await expenseDao.getAllExpenses();
    expect(expenses.length).toBeGreaterThanOrEqual(3);
});

// Tests the getExpenseById function in the ExpenseDAO.
test("Get expense by id", async () =>
{
    let expense:Expense = new Expense(1, 4, "for more chairs", 200, "");
    expense = await expenseDao.createExpense(expense);
    let retrievedExpense:Expense = await expenseDao.getExpenseById(expense.id);
    expect(retrievedExpense.id).toBe(expense.id);
});

// Tests the updateExpense function in the ExpenseDAO.
test("Update expense", async () =>
{
    let expense:Expense = new Expense(1, 2, "for more chairs", 200, "");
    expense = await expenseDao.createExpense(expense);
    expense.amount = 100;
    expense = await expenseDao.updateExpense(expense);
    expect(expense.amount).toBe(100);
});

// Tests the deleteExpenseById function in the ExpenseDAO.
test("Delete expense by id", async () =>
{
    let expense:Expense = new Expense(1, 1, "for more chairs", 200, "");
    expense = await expenseDao.createExpense(expense);
    const result:boolean = await expenseDao.deleteExpenseById(expense.id);
    expect(result).toBeTruthy();
});

// Closes the connection after the tests finish.
afterAll(async () =>
{
    conn.end();
});